from flask import Flask
from flask import request

app = Flask(__name__)


@app.route("/")
def index():
    num1 = request.args.get("num1", "")
    num2 = request.args.get("num2", "")
    if num1 and num2:
        Soma = fahrenheit_from(num1, num2)
    else:
        Soma = ""

    return (
        """<h2> DevOps ATP Bruno Sena e Cesar Silva </h2>"""
        """<br>"""
        """<form action="" method="get">
                <input type="text" name="num1">
                <br>
                <input type="text" name="num2">
                <br>
                <input type="submit" value="Soma">
            </form>"""
        + "Soma: "
        + '<a id="fahrenheit">' + Soma + '</a>'

    )


@app.route("/<int:celsius>")
def fahrenheit_from(num1, num2):
    """Convert Celsius to Fahrenheit degrees."""
    return str(int(num1)+int(num2))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
    #  app.run(host="0.0.0.0", port=8080, debug=False)
